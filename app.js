const express = require('express')
const bodyParser=require('body-parser');
const cors=require('cors');
const app = express()
const port = 30000
app.use(cors());
app.use(bodyParser.json());
app.get('/',(req,res)=>{
  res.send('Hell From Service 2 ')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
